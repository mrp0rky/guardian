package com.guardian.commands;

import com.google.gson.JsonObject;
import com.guardian.Command;
import com.guardian.util.ImageLayoutGenerator;
import com.guardian.util.RestReturn;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class OsuAPIRequest implements Command {
    private final String HELP = "Usage: !osu";

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        RestReturn restReturn = new RestReturn();
        JsonObject valueOut = null;
        if (args.length == 1) {
            if (args[0].contains("/s/")) {
                valueOut = restReturn.getRest("https://osu.ppy.sh/api/get_user?u=" + args[0] + "&k=a5bd04d148391aecf3baebd376d2ad912c311c00");
            }else{
                valueOut = restReturn.getRest("https://osu.ppy.sh/api/get_user?u=" + args[0] +"&k=a5bd04d148391aecf3baebd376d2ad912c311c00");
            }
        }

        System.out.println(valueOut);

        ImageLayoutGenerator imageLayoutGenerator = new ImageLayoutGenerator("res/osu_background.png", new Color(255, 102, 170));

        imageLayoutGenerator.addHeader(valueOut.get("username").toString().substring(1, valueOut.get("username").toString().length() - 1));
        String ppString = valueOut.get("pp_raw").toString().substring(1, valueOut.get("pp_raw").toString().length() - 1).split("\\.", 2)[0];
        imageLayoutGenerator.addContent(ppString + "pp");

        File file = new File("res/output.png");
        try {
            ImageIO.write(imageLayoutGenerator.generateImage(), "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        event.getTextChannel().sendFile(file).queue();

    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        help_bulder.setTitle("Ping", null);
        help_bulder.setDescription("Tests for if the bot is currently responsive");
        help_bulder.addField("Syntax", "!ping \n!ping --help", true);
        help_bulder.addField("", "{Pings the bot}\n{Requests help for the bot}", true);
        help_bulder.addField("Permissions", "Default", false);

        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
