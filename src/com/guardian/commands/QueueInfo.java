package com.guardian.commands;

import com.guardian.Command;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.LinkedList;

public class QueueInfo implements Command {
    private final String HELP = "Usage: !queue";

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        VoiceChannel channel = getUserVoiceChannel(event);
        if (channel != null) {
            final GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
            LinkedList<AudioTrack> queue = musicManager.scheduler.getQueue();

            long trackduration;
            try {
                trackduration = musicManager.player.getPlayingTrack().getInfo().length - musicManager.player.getPlayingTrack().getPosition();

                for (AudioTrack track : queue) {
                    trackduration += track.getInfo().length;
                }
            }catch (NullPointerException e){
                trackduration = 0;
            }
            trackduration = trackduration/ 1000;

            /*
            Calculating the time till the next song / value
             */

            long hoursToComplete = trackduration / 3600;
            long minutesToComplete = ((trackduration) / 60) % 60;
            long secondsToComplete = trackduration % 60;
            System.out.println(hoursToComplete);
            String timeToComplete;
            if(hoursToComplete != 0) {
                timeToComplete = String.format("%02d:%02d:%02d", hoursToComplete, minutesToComplete, secondsToComplete);
            }else if(minutesToComplete != 0){
                timeToComplete = String.format("%02d:%02d", minutesToComplete, secondsToComplete);
            }else{
                timeToComplete = String.format("%02d", secondsToComplete);
            }

            long currentSong = (musicManager.player.getPlayingTrack().getInfo().length - musicManager.player.getPlayingTrack().getPosition()) / 1000;
            long minutesTillNext = ((currentSong) / 60) % 60;
            long secondsTillNext =  (currentSong % 60);
            String timeToNext;

            if(minutesTillNext != 0){
                timeToNext = String.format("%02d:%02d", minutesTillNext, secondsTillNext);
            }else{
                timeToNext = String.format("%02d", secondsTillNext);
            }

            EmbedBuilder queue_builder = new EmbedBuilder();
            queue_builder.setColor(event.getMember().getColor());
            queue_builder.setTitle("Current queue for " + event.getGuild().getName(), null);
            queue_builder.setThumbnail("https://img.youtube.com/vi/" + musicManager.player.getPlayingTrack().getInfo().identifier +"/hqdefault.jpg");
            queue_builder.setFooter("Time till finished: " + timeToComplete, null);
            queue_builder.setDescription("Time till next song: " + timeToNext);
            queue_builder.addField("", "Currently playing - " + musicManager.player.getPlayingTrack().getInfo().title + " - " + musicManager.player.getPlayingTrack().getInfo().author, false);
            for(int i = 0; i < 5 && i < queue.size(); i++){
                queue_builder.addField("", i + 1 + ". - " + queue.get(i).getInfo().title  + " - " + queue.get(i).getInfo().author, false);
            }
            // Build the Message
            MessageEmbed embed = queue_builder.build();
            MessageBuilder mbuilder = new MessageBuilder();
            mbuilder.setEmbed(embed);
            Message message_compiled = mbuilder.build();

            // Output the message
           event.getChannel().sendMessage(message_compiled).queue();
        } else {
            event.getTextChannel().sendMessage("You must be connected to the voice channel to request the queue!").queue();
        }
    }

    private VoiceChannel getUserVoiceChannel(MessageReceivedEvent event) {
        if(event.getMember().getVoiceState().inVoiceChannel()){
            return event.getMember().getVoiceState().getChannel();
        }
        return null;
    }

    public synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = PlaySong.musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(PlaySong.playerManager);
            PlaySong.musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
