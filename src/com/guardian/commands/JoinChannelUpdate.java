package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import com.guardian.util.Config;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.sql.*;

public class JoinChannelUpdate implements Command{
    /*
    This is not for updating the channel itself when a new user joins -> This is to update the database on the change that has been made such that this is a moderation
    tool
     */
    private final String HELP = "Usage: !ping";
    private Connection connect = null;
    private Statement statement = null;

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        if(CommandPermission.isOwner(event)){
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                connect = DriverManager
                        .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true"
                                , Config.sqlUser, Config.sqlPassword);
                statement = connect.createStatement();
                String queryCheck = "SELECT * from guilds WHERE gid = " + Long.parseLong(event.getGuild().getId());
                Statement qc = connect.createStatement();
                ResultSet qc_rs = statement.executeQuery(queryCheck);
                if (qc_rs.absolute(1)) {
                    /*
                    I use prepared statements here to exit any user input - This removes the threat of SQL Injection
                    Or at-least StackOverflow told me it does...
                     */
                    PreparedStatement updatedescription = connect.prepareStatement("UPDATE guilds SET welcome_channel=(?) WHERE gid =" + Long.parseLong(event.getGuild().getId()));
                    updatedescription.setString(1, event.getTextChannel().getName());
                    updatedescription.executeUpdate();
                    return;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}

