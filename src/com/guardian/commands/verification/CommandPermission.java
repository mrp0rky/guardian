package com.guardian.commands.verification;

import com.guardian.util.Config;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.apache.commons.collections4.CollectionUtils;

import java.sql.*;
import java.util.*;

public class CommandPermission {

    public static boolean hasPermission(String requiredRole, MessageReceivedEvent event) {
        Member caller = event.getGuild().getMember(event.getAuthor());
        List roles = caller.getRoles();
        List<Role> role = event.getGuild().getRolesByName(requiredRole, true);

        return roles.contains(role.get(0)) && event.getGuild() != null;

    }

    public static boolean isAdmin(MessageReceivedEvent event) {
        /*Connection connect;
        Statement statement;
        String adminRoles = "";
        ArrayList<String> callerRoles = new ArrayList<>(), adminRoleArray = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true"
                            , Config.sqlUser, Config.sqlPassword);
            statement = connect.createStatement();

            ResultSet rs = statement.executeQuery("SELECT adminRoles FROM guilds WHERE gid = " + event.getGuild().getId());

            while (rs.next()) {
                adminRoles = rs.getString("adminRoles");
            }
            adminRoleArray = new ArrayList<>(Arrays.asList(adminRoles.split(",")));
            Member caller = event.getGuild().getMember(event.getAuthor());
            for(int i = 0; i < caller.getRoles().size(); i++){
                String tempRoles = caller.getRoles().get(i).getName();
                tempRoles = tempRoles.replace("[", "").replace("]", "").replaceAll(", ", "\n");
                callerRoles.add(callerRoles.size(), tempRoles);
            }
            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assert adminRoleArray != null;
        return CollectionUtils.containsAny(adminRoleArray, callerRoles);*/
        return event.getMember().getPermissions().contains(Permission.ADMINISTRATOR);
    }

    public static boolean isOwner(MessageReceivedEvent event) {
        if (event.getAuthor().getId().equals(event.getGuild().getOwner().getUser().getId())) {
            return true;
        } else {
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - You do not have permission to change this! Required permission: PERMISSION_OWNER").queue();
            return false;
        }

    }
}

