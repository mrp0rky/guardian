package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import com.guardian.util.Config;
import com.guardian.util.MusicMon;
import com.guardian.util.RestReturn;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.managers.AudioManager;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
    Welcome to insanity. This is by far the most complex part of Guardian.
    "No one knew adding a music bot was this hard" - Donald "#1946" Trump
 */

public class PlaySong extends ListenerAdapter implements Command {
    private final String HELP = "Usage: !ping";
    private boolean search = false;
    private MessageReceivedEvent location;
    private LinkedHashMap<String, Integer> channelsSkip = new LinkedHashMap<>();

    public static AudioPlayerManager playerManager = new DefaultAudioPlayerManager();
    public static Map<Long, GuildMusicManager> musicManagers = new HashMap<>();

    private Connection connect = null;
    private Statement statement = null;

    public synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(playerManager);
            musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);
        if (event.getMessage().getContentDisplay().equals("!skip")) {
            skipTrack(event.getTextChannel(), event);
        } else {
            String[] command = event.getMessage().getContentDisplay().split(" ", 2);
            try {
                URL url = new URL(command[1]);
                Guild guild = event.getGuild();
                TextChannel channel = event.getTextChannel();
                if (guild != null) {
                    loadAndPlay(event.getTextChannel(), command[1], event);
                }
            } catch (MalformedURLException e) {
                searchForVideo(event);
            }
        }

        super.onMessageReceived(event);
        return true;
    }

    private void loadAndPlay(final TextChannel channel, final String trackUrl, MessageReceivedEvent event) {
        final GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
        //musicManager.player.setVolume(50);
        playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                play(channel.getGuild(), musicManager, track, event);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                AudioTrack firstTrack = playlist.getSelectedTrack();
                if (firstTrack == null) {
                    firstTrack = playlist.getTracks().get(0);
                }
                channel.sendMessage(event.getAuthor().getAsMention() + " - Added " + firstTrack.getInfo().title + " (first track of playlist " + playlist.getName() + ")").queue();

                play(channel.getGuild(), musicManager, firstTrack, event);
            }

            @Override
            public void noMatches() {
                channel.sendMessage(event.getAuthor().getAsMention() + " - No songs were found  with the URL: " + trackUrl).queue();
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                if (!search) {
                    channel.sendMessage("Could not play: " + exception.getMessage()).queue();
                } else {
                    channel.sendMessage(event.getAuthor().getAsMention() + " - Retried the most popular song from the search query and added it to the queue!").queue();
                }
            }
        });
    }

    private void play(Guild guild, GuildMusicManager musicManager, AudioTrack track, MessageReceivedEvent event) {
        try {
            AudioManager audioManager = guild.getAudioManager();
            boolean connected = false;
            for (int i = 0; i < audioManager.getGuild().getVoiceChannels().size(); i++) {
                VoiceChannel voiceChannel = audioManager.getGuild().getVoiceChannels().get(i);
                long songlength = track.getDuration() / 1000;
                if (voiceChannel.getMembers().contains(event.getMember())) {
                    connected = true;
                    if (songlength < 3600 && !track.getInfo().isStream) {
                        connectToFirstVoiceChannel(guild, guild.getAudioManager(), event, voiceChannel);

                        musicManager.scheduler.queue(track);

                        try{
                            connect = DriverManager.getConnection("jdbc:mysql://" + Config.sqlHostname + ":3306/" + Config.sqlUser + "?autoReconnect=true&useSSL=false&serverTimezone=UTC"
                                    , Config.sqlUser, Config.sqlPassword);
                            statement = connect.createStatement();

                            ResultSet rs = statement.executeQuery("SELECT bot_volume FROM guilds WHERE gid = " + event.getGuild().getId());
                            int volume = 0;
                            while (rs.next()) {
                                volume = rs.getInt("bot_volume");
                            }
                            musicManager.player.setVolume(volume);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }finally {
                            musicManager.player.setVolume(15);
                        }
                        if (!search) {
                            // Check if the song is requested by a URL or via a search -> Determine what is to be done with the song
                            event.getTextChannel().sendMessage("Added " + track.getInfo().title + " to the queue as requested by " + event.getAuthor().getAsMention()).queue();
                        } else {
                            event.getTextChannel().sendMessage("Added " + track.getInfo().title + " to the queue from a search by " + event.getAuthor().getAsMention()).queue();
                        }
                    } else if (track.getInfo().isStream) {
                        event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Livestreams are currently disabled!").queue();
                    } else {
                        event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - The song length must not exceed one hour!").queue();
                    }
                }
            }
            if (!connected) {
                event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - You must be in a voice channel to add a song to the queue").queue();
            }

        } catch (Exception e) {
            location.getTextChannel().sendMessage("Failed to Play " + track.getInfo() + ": Song URL Invalid").queue();
        }

    }

    private static void connectToFirstVoiceChannel(Guild guild, AudioManager audioManager, MessageReceivedEvent event, VoiceChannel voiceChannel) {
        if (!audioManager.isConnected() && !audioManager.isAttemptingToConnect()) {
            audioManager.openAudioConnection(voiceChannel);
            new MusicMon(guild, audioManager);
        }
    }

    private void skipTrack(TextChannel channel, MessageReceivedEvent event) {
        Guild guild = event.getGuild();
        String[] command = event.getMessage().getContentDisplay().split(" ", 2);
        try {
            if ((CommandPermission.hasPermission("Bot Commander", event) || CommandPermission.isAdmin(event)) && guild != null) {
                if ("!skip".equals(command[0])) {
                    try {
                        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
                        musicManager.scheduler.nextTrack();
                        musicManager.scheduler.getQueue();
                        channel.sendMessage(event.getAuthor().getAsMention() + " - Skipped to next track.").queue();
                    } catch (Exception e) {
                        event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + ": Failed to skip the track - Please ensure you have a request in the queue!").queue();
                    }
                }
            } else {
                final boolean[] hasSkipped = {false};
                final long[] message_time = {event.getMessage().getCreationTime().toEpochSecond()};
                channel.getHistory().retrievePast(100).queue((List<Message> messages) -> {
                    for (int x = 1; x < messages.size(); x++) {

                        if (messages.get(x).getAuthor() == event.getAuthor() && messages.get(x).getContentDisplay().equals("!skip")) {
                            long search_message_time = messages.get(x).getCreationTime().toEpochSecond();
                            int length = 180;
                            if ((message_time[0] - search_message_time) > length) {
                                for (int i = 0; i < event.getGuild().getVoiceChannels().size(); i++) {
                                    VoiceChannel voiceChannel = event.getGuild().getVoiceChannels().get(i);
                                    if (voiceChannel.getMembers().contains(event.getJDA().getSelfUser())) {
                                        int connectedUserSize = voiceChannel.getMembers().size(), requiredSkips = (int) Math.floor(connectedUserSize / 1.5), update = 1;
                                        if (!channelsSkip.containsKey(voiceChannel.getId())) {
                                            channelsSkip.put(voiceChannel.getId(), 1);
                                        } else {
                                            Set<Map.Entry<String, Integer>> set = channelsSkip.entrySet();
                                            for (Map.Entry<String, Integer> me : set) {
                                                if (me.getKey().equals(voiceChannel.getId())) {
                                                    update = me.getValue();
                                                    update++;
                                                    channelsSkip.replace(me.getKey(), update);
                                                }
                                            }
                                        }

                                        if (update == requiredSkips) {
                                            if ("!skip".equals(command[0])) {
                                                try {
                                                    GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
                                                    musicManager.scheduler.nextTrack();
                                                    channel.sendMessage(event.getAuthor().getAsMention() + " - Skipped by vote!").queue();
                                                    channelsSkip.remove(event.getGuild().getId());
                                                } catch (Exception e) {
                                                    event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + ": Failed to skip the track - Please ensure you have a request in the queue!").queue();
                                                }
                                            }
                                        } else {
                                            event.getTextChannel().sendMessage(event.getAuthor().getName() + " has voted to skip the current track! Amount of skips needed: " + update + "/" + requiredSkips).queue();
                                        }
                                    }
                                }
                                break;
                            } else {
                                event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - You have already voted! Votes can only be cast every " + length + " seconds!").queue();
                                break;
                            }
                        }
                    }
                });
                if (!hasSkipped[0]) {

                }
            }
        } catch (IndexOutOfBoundsException e) {
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - No users defined as Bot Commanders! Please tell the server owner to create the role" +
                    ", Bot Commander!").queue();
        }

    }

    public void searchForVideo(MessageReceivedEvent event) {
        String[] command = event.getMessage().getContentDisplay().split(" ", 7);      // Split up the song into it's various parameters MAX: 7
        String search_query = "";
        if (command.length < 13) {
            for (int i = 1; i < command.length; i++) {
                if (i == 1) {
                    search_query += command[i];
                } else {
                    search_query += ("%20" + command[i]);
                }
            }
            String youtube_url = "";
            Pattern p = Pattern.compile(".*\\\"(.*)\\\".*");
            RestReturn restReturn = new RestReturn();
            Matcher m = p.matcher(restReturn.getRestYoutube("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=5&key=AIzaSyDgAM4VdRPjVUyvdbH5rwM2-xWVS9YhKH4&q=" + search_query));



            while (m.find()) {
                youtube_url = "https://www.youtube.com/watch?v=" + m.group(1);
                break;
            }

            if (event.getGuild() != null) {
                search = true;
                loadAndPlay(event.getTextChannel(), youtube_url, event);
            }
        } else {
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Error: Query was too long! Please remove any unnecessary words and try again!").queue();
        }
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {

    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        if (!event.getMessage().getContentDisplay().toString().equals("!skip")) {
            help_bulder.setTitle("Music Player", null);
            help_bulder.setDescription("Plays the currently requested song");
            help_bulder.addField("Syntax", "!play {song URL}\n!play {search query}\n!play --help", true);
            help_bulder.addField("", "{Plays the currently requested song}\n{Searches for the most relevant song for the bot to play}\n{Requests help for the bot}", true);
            help_bulder.addField("Permissions", "Default", false);
            help_bulder.addField("Additional Infomation", "If URL is requested, the URL must be a valid audio source", false);
        } else {
            help_bulder.setTitle("Skip song", null);
            help_bulder.setDescription("Skips the currently playing song");
            help_bulder.addField("Syntax", "!skip}\n!skip --help", true);
            help_bulder.addField("", "{Skips the currently playing song}\n{Requests help for the bot}", true);
            help_bulder.addField("Permissions", "Bot Commander+", false);
        }

        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }
}