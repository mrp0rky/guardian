package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import com.guardian.util.SchedulerService;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.managers.AudioManager;

import javax.sound.midi.Track;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SkipSong implements Command {

    private final String HELP = "Usage: !skip";
    private AudioPlayerManager playerManager;
    private Map<Long, GuildMusicManager> musicManagers;

    private synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(playerManager);
            musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }


    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        this.musicManagers = new HashMap<>();

        this.playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);


        Guild guild = event.getGuild();
        long guildId = Long.parseLong(guild.getId());
        /*
        This works. Leave it.
        Don't. Touch. Any. Part. Of. This. Ever. Again.
        */
       String[] command = event.getMessage().getContentDisplay().split(" ", 2);

        if (CommandPermission.hasPermission("Bot Commander", event) && guild != null) {
            if ("!skip".equals(command[0])) {
                try {

                } catch (Exception e) {
                    event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + ": Failed to skip the track - Please ensure you have a request in the queue!").queue();
                }
            }
        } else {
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + ": Error - Permission denied - Please make sure you are the role [Bot Commander]!").queue();
        }

    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }
}
