package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import com.guardian.util.BotSQLQuery;
import com.guardian.util.Config;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class AddAdminRole implements Command {
    private final String HELP = "Usage: !ping";
    Connection connect = null;
    Statement statement = null;
    PreparedStatement ps = null;

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        if (CommandPermission.isOwner(event)) {
            List<Role> mentionedRoles = event.getMessage().getMentionedRoles();
            LinkedList<String> roles = new LinkedList<>();
            for (int i = 0; i < mentionedRoles.size(); i++) {     // Add the role to the SQL query string -> NOT SAFE!
                roles.add(i, mentionedRoles.get(i).getName());
            }
            String sqlEntry = roles.toString().replaceAll("\\[", "").replaceAll("\\]", "");
            if(!Config.DISABLESQL){
                BotSQLQuery sqlQuery = new BotSQLQuery();
                sqlQuery.setSQLEntry("guilds","adminRoles", sqlEntry,"gid",event.getGuild().getId());
            }
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                connect = DriverManager
                        .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true"
                                , Config.sqlUser, Config.sqlPassword);
                PreparedStatement updateadmin = connect.prepareStatement("UPDATE guilds SET adminRoles=(?) WHERE gid =" + event.getGuild().getId());
                updateadmin.setString(1, sqlEntry);
                updateadmin.executeUpdate();
                connect.close();
                return;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Failed to add specific role to ").queue();
        }
    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        help_bulder.setTitle("Add Admin role",null);
        help_bulder.setDescription("Adds the requested role to be added as an Admin");
        help_bulder.addField("Syntax", "!addadminrole [role to add] {Adds the current role that has been requested as an admin}\n!addadminrole --help {Requests help for the syntax of the command}",true);
        help_bulder.addField("Permissions","Owner+", false);
        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
