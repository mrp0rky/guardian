package com.guardian.commands;

import com.guardian.util.Config;
import com.guardian.util.SQLQuery;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.sql.ResultSet;
import java.sql.SQLException;

public class QuoteManager {
    private SQLQuery query;
    public QuoteManager(){
        query = new SQLQuery(Config.sqlHostname, Config.sqlDatabaseName, Config.sqlUser, Config.sqlPassword, 3306);
    }

    public void addQuote(Message message){
        String[] values = {message.getId(), message.getAuthor().getId(), message.getGuild().getId(), message.getContentRaw()};
        query.query("INSERT INTO quotes (messageid, uid, guild_id, message_content) VALUES (?, ?, ? ,?)", values);
    }

    public void addQuote(User user, String message, Guild guild, Message messageRecieved){
        String[] values = {messageRecieved.getId(), user.getId(), guild.getId(), message};

        query.query("INSERT INTO quotes (messageid, uid, guild_id, message_content) VALUES (?, ?, ? ,?)", values);
    }

    public String getMessageByUser(MessageReceivedEvent event){
        User users = event.getMessage().getMentionedUsers().get(0);
        String[] values = {users.getId()};
        ResultSet rs = query.query("SELECT * FROM quotes WHERE uid = ? ", values);
        try {
                return rs.getString("message_content");
        }catch (SQLException e){
            return null;
        }
    }
}
