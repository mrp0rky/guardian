package com.guardian.commands;

import com.guardian.Command;
import com.guardian.util.Config;
import com.mysql.cj.api.mysqla.result.Resultset;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.sql.*;

public class DescriptionUpdater implements Command {
    private final String HELP = "Usage: !description [description]";
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        String message_raw = event.getMessage().getContentRaw();
        String description = message_raw.replaceFirst("!description ", "");
        if((description.equals("!description") == false) && (description.length() < 200)) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                connect = DriverManager
                        .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true&serverTimezone=UTC"
                                , Config.sqlUser, Config.sqlPassword);
                statement = connect.createStatement();
                String queryCheck = "SELECT * from users WHERE id = " + Long.parseLong(event.getAuthor().getId());
                Statement qc = connect.createStatement();
                ResultSet qc_rs = statement.executeQuery(queryCheck);
                if (qc_rs.absolute(1)) {
                    /*
                    I use prepared statements here to exit any user input - This removes the threat of SQL Injection
                    Or at-least StackOverflow told me it does...
                     */
                    PreparedStatement updatedescription = connect.prepareStatement("UPDATE users SET description=(?) WHERE id =" + Long.parseLong(event.getAuthor().getId()));
                    updatedescription.setString(1, description);
                    updatedescription.executeUpdate();
                    event.getMessage().getChannel().sendMessage(event.getAuthor().getAsMention() + "Successfully changed description to: " + description).queue();
                    return;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            if(description.length() < 200) {
                event.getMessage().getChannel().sendMessage(event.getAuthor().getAsMention() + "Error! Please specify a valid description!").queue();
            }else{
                event.getMessage().getChannel().sendMessage(event.getAuthor().getAsMention() + "Error! Your description is too long! Please specify a description <200 characters long!").queue();
            }
        }
    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        help_bulder.setTitle("Description Update",null);
        help_bulder.setDescription("Updates the users description");
        help_bulder.addField("Syntax", "!description [new description]\n!description --help",true);
        help_bulder.addField("", "{Changes the current description of the user}\n{Requests help for the bot}",true);
        help_bulder.addField("Permissions","Default", false);

        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
