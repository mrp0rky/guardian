package com.guardian.commands;

import com.guardian.Command;
import com.guardian.util.Config;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.entities.Role;

import java.sql.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;

public class ProfileInfo implements Command {
    private final String HELP = "Usage: !profile / !profile [user]";
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private int xp;
    private String description = "Default Description";

    private User requested_user;
    private Member requested_member;
    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        String message_raw = event.getMessage().getContentRaw();
        String profile_name = message_raw.replaceFirst("!profile ", "");
        if (message_raw.equals("!profile")) {
            requested_user = event.getAuthor();
            requested_member = event.getMember();
        } else {
            /*
            For other people's profiles
             */
            List mentionedUsers = event.getMessage().getMentionedUsers();
            requested_user = (User) mentionedUsers.get(0);
            requested_member = event.getGuild().getMemberById(requested_user.getId());
        }
        addSQLEntry(event);

        Message message = event.getMessage();
        String[] messageArgs = message.toString().split(" ");
        EmbedBuilder status_builder = new EmbedBuilder();
            /*
            Nice and simple, this should be for the user themselves that is requesting the profile info
            For reference here, the profile is event.getAuthor() as opposed to the index of 1.
             */

        // Embed Builder Settings
        status_builder.setTitle(requested_user.getName(), null);
        status_builder.setFooter(requested_user.getName().toString() + " joined Discord on " + requested_user.getCreationTime().format(DateTimeFormatter.RFC_1123_DATE_TIME), null);
        status_builder.setThumbnail(requested_user.getAvatarUrl());
        status_builder.setColor(event.getMember().getColor());
        // Content
        if (requested_member.getOnlineStatus().toString() == "ONLINE") {
            status_builder.setDescription("Currently online" + "\n\"" + description + "\"");
        } else if (requested_member.getOnlineStatus().toString() == "IDLE") {
            status_builder.setDescription("Currently Idling" + "\n\"" + description + "\"");
        } else if (requested_member.getOnlineStatus().toString() == "DO_NOT_DISTURB") {
            status_builder.setDescription("Do not disturb" + "\n\"" + description + "\"");
        } else if (requested_member.getOnlineStatus().toString() == "OFFLINE") {
            status_builder.setDescription("Currently Offline" + "\n\"" + description + "\"");
        }

        // Adds the roles to the profile embedded builder
        List<Role> role = event.getGuild().getRoles();
        List roles = requested_member.getRoles();
        LinkedList userRole = new LinkedList();
        String roles_out;
        for (int i = 0; i < role.size(); i++) {
            if (roles.contains(role.get(i))) {
                userRole.add(userRole.size(), role.get(i).getName());
            }
        }
        roles_out = userRole.toString().replace("[", "").replace("]", "").replaceAll(", ", "\n");

        // Calculations for the user level from the XP
        long level = (long) (Math.pow(xp, 1 / 3.35));
        double xp_min = Math.pow(level, 3.35);
        double xp_max = Math.pow(level + 1, 3.35);
        double percent = ((xp - xp_min) / (xp_max - xp_min)) * 100;
        String percent_string = "[";
        char percent_indicator = '=';
        for (int i = 0; i <= 100 / 3; i++) {
            if (i < Math.ceil(percent / 3)) {
                percent_string = percent_string + percent_indicator;
            } else {
                percent_string = percent_string + " ";
            }
        }
        percent_string = percent_string + "]";
        // Adds the content itself to the profile card
        status_builder
                .addField("General Stats:", "Joined " + event.getGuild().getName() + "\n on: " + event.getGuild().getMember(requested_user)
                        .getJoinDate().format(DateTimeFormatter.RFC_1123_DATE_TIME), true).addField("Roles:", roles_out.toString(), true)
                .addField("Other Info", "XP: " + xp + "/" + (int) Math.ceil(xp_max) + "                                          Currently Level: " + level + "\nLevel Progress: "
                        + percent_string, true);

        // Build the Message
        MessageEmbed embed = status_builder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();

    }



    public void addSQLEntry(MessageReceivedEvent event) {
        /*
        Loads the database into memory - Can be used for committing information
         */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true&serverTimezone=UTC"
                            , Config.sqlUser, Config.sqlPassword);
            statement = connect.createStatement();

            ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE id = " + Long.parseLong(requested_user.getId()));

            while (rs.next()) {
                description = rs.getString("description");
                xp = rs.getInt("xp");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
