package com.guardian.commands;

import com.guardian.Command;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Status implements Command {
    private final String HELP = "Usage: !ping";
    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        JDA jda = event.getJDA();
        List<Guild> guilds = jda.getGuilds();

        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
        long uptime = rb.getUptime();
        long days = TimeUnit.MILLISECONDS.toDays((uptime));
        long hours = TimeUnit.MILLISECONDS.toHours((uptime));
        long minutes = TimeUnit.MILLISECONDS.toMinutes(uptime);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(uptime);
        EmbedBuilder status_builder = new EmbedBuilder();
        status_builder.setColor(event.getMember().getColor());
        /*
        Don't hate me for this poorly optimised if statement block
        TODO - FIX THIS IN-CASE SOMEONE FROM /r/ProgrammerHumor sees this
         */
        if(minutes == 0){
            status_builder.setDescription("Guardian has currently been online for " + (seconds - (minutes * 60)) + " seconds");
        }else if(hours == 0){
            status_builder.setDescription("Guardian has currently been online for " + (minutes - (hours * 60)) + " minutes, " + (seconds - (minutes * 60)) + " seconds");
        }else if(days == 0){
            status_builder.setDescription("Guardian has currently been online for " + (hours - (days * 24)) + " hours, "
                    + (minutes - (hours * 60)) + " minutes, " + (seconds - (minutes * 60)) + " seconds");
        }else if(days != 0 ) {
            status_builder.setDescription("Guardian has currently been online for " + days + " days, " + (hours - (days * 24)) + " hours, "
                    + (minutes - (hours * 60)) + " minutes, " + (seconds - (minutes * 60)) + " seconds");
        }
        status_builder.setTimestamp(Instant.now());
        status_builder.setThumbnail(jda.getSelfUser().getAvatarUrl());
        status_builder.setTitle("-- Guardian Status --", null);
        status_builder.setFooter("Guardian is a bot Programmed by Mrporky#0325", null);
        MessageEmbed embed = status_builder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        status_builder.addField("Name:", event.getJDA().getSelfUser().getName() + " (ID: " + event.getJDA().getSelfUser().getId() + ")", true)
                .addField("Statistics", "Guardian is currently active on " + guilds.size() + " total servers!", true )
       .addField("Current Version:", "JDA-3.1.1.224 - Bot Version: 130817v1", false);
        Message message = mbuilder.build();
        event.getChannel().sendMessage(message).queue();
    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }
}
