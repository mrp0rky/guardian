package com.guardian.commands;

import com.guardian.Command;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.List;
import java.util.Vector;

public class Slowmode implements Command {
    private final String HELP = "Usage: !slowmode [time between messages]";
    public static int slow_mode_length = 30;
    public static Vector<String> slowmode_channels = new Vector<>(10, 2);
    public static Vector<Integer> slowmode_length = new Vector<>(10, 2);
    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        Member member = event.getGuild().getMember(event.getAuthor());
        List roles = member.getRoles();
        List<Role> role = event.getGuild().getRolesByName("Bot Commander", true);

        if (roles.contains(role.get(0)) && event.getGuild() != null) {
            String message = event.getMessage().getContentDisplay();
            String trim = message.trim();
            String message_split[] = message.split(" ");

            if (!trim.isEmpty()) {
                if (trim.split("\\s+").length == 1) {
                    // Default slowmode -> 30 seconds
                    slow_mode_length = 30;
                    event.getTextChannel().sendMessage("This channel is now in slow-mode! Users may only send messages every " + slow_mode_length + " seconds!").queue();
                    slowmode_channels.addElement(event.getTextChannel().getId());
                } else if (message_split[1].equalsIgnoreCase("off") && slowmode_channels.contains(event.getTextChannel().getId())) {
                    event.getTextChannel().sendMessage("This channel is no longer in slow-mode! Play nicely!").queue();
                    slowmode_channels.removeElement(event.getTextChannel().getId());
                } else if(message_split[1] != null){
                    // For custom lengths
                    slowmode_channels.addElement(event.getTextChannel().getId());

                    slow_mode_length = Integer.parseInt(message_split[1]);
                    slowmode_length.add(slowmode_channels.indexOf(event.getTextChannel().getId()), slow_mode_length);
                    event.getTextChannel().sendMessage("This channel is now in slow-mode! Users may only send messages every " + slow_mode_length + " seconds!").queue();

                }else {
                    event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Invalid command! Correct format is [!slowmode] or [!slowmode off]").queue();

                }

            }
        }else{
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + ": Error - Permission denied - Please make sure you are the role [Bot Commander]!").queue();
        }

    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
