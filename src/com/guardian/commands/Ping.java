package com.guardian.commands;

import com.guardian.Command;
import com.guardian.util.ImageLayoutGenerator;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Ping implements Command {
    private final String HELP = "Usage: !ping";

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        event.getTextChannel().sendMessage("Pong!").queue();
    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        help_bulder.setTitle("Ping", null);
        help_bulder.setDescription("Tests for if the bot is currently responsive");
        help_bulder.addField("Syntax", "!ping \n!ping --help", true);
        help_bulder.addField("", "{Pings the bot}\n{Requests help for the bot}", true);
        help_bulder.addField("Permissions", "Default", false);

        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
