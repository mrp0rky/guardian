package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PruneUser implements Command {
    private final String HELP = "Usage: !prune [@{username}]";

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        Message message = event.getMessage();
        String message_content = message.getContentDisplay();
        ArrayList message_array = new ArrayList<String>(Arrays.asList(message_content.split(" ")));
        User requested_user;

        List mentionedUsers = event.getMessage().getMentionedUsers();
        requested_user = (User) mentionedUsers.get(0);  // Gets user of index 0, as this will always be the first user in the message, will ignore all other users

        if (CommandPermission.isAdmin(event)) {
            message_content = message_content.replaceFirst("!prune ", "");  // Remove the command
            TextChannel channel = event.getTextChannel();
            try {
                if (!message_array.get(2).toString().contains("@") && Integer.parseInt(message_array.get(2).toString()) < 100) {
                    channel.getHistory().retrievePast(Integer.parseInt(message_array.get(2).toString())).queue((List<Message> messages) -> {
                        for (int i = 0; i < messages.size() && i < 100; i++) {
                            if (messages.get(i).getAuthor() == requested_user) {
                                messages.get(i).delete().queue();
                            }
                        }
                    });
                    event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Pruned " + message_array.get(2) + " messages from " + requested_user.getName()).queue();
                } else {
                    event.getTextChannel().sendMessage("Invalid syntax! Correct syntax is !prune {user} {messages to prune}").queue();
                }
            } catch (NumberFormatException e) {
                event.getTextChannel().sendMessage("The number of messages must be less than 100 and must be a valid integer!").queue();
            }
        }


    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }
}
