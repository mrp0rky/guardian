package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.managers.GuildManager;

import java.util.List;

public class KickUser implements Command {
    private final String HELP = "Usage: !kick [user] [reason]";

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        if (CommandPermission.isAdmin(event) | CommandPermission.hasPermission("Bot Commander", event)){
            String message = event.getMessage().getContentDisplay().toString();
            String message_split[] = message.split(" ");
            Guild guild = event.getGuild();

            GuildManager guild_manager = event.getGuild().getManager();
            if (guild.getTextChannelsByName("mod-log", false).isEmpty()) {
                guild_manager.getGuild().getController().createTextChannel("mod-log").queue();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                event.getGuild().getTextChannelsByName("mod-log", false).get(0).sendMessage(guild.getOwner().getAsMention() + " - No text channel, #mod-log existed! Created one!").queue();
            }

            if (message_split.length == 3) {
                Member member = event.getGuild().getMember(event.getMessage().getMentionedUsers().get(0));
                guild_manager.getGuild().getController().kick(member).queue();
                guild.getTextChannelsByName("mod-log", false).get(0).sendMessage(member.getAsMention() + " was kicked by " + event.getAuthor().getAsMention() + " for " + message_split[2]).queue();
            } else {
                event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Invalid Command: Correct syntax: !kick [user] [reason]!").queue();
            }
        }else{
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - You do not have permission to kick a member!").queue();
        }
    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        help_bulder.setTitle("Kick User",null);
        help_bulder.setDescription("Kicks the requested user");
        help_bulder.addField("Syntax", "!kick [user] \n!kick --help",true);
        help_bulder.addField("", "{Kicks the @user-ref}\n{Requests help for the bot}",true);
        help_bulder.addField("Permissions","Admin+", false);
        help_bulder.addField("Additional Infomation","Will output the kick to a channel, #mod-log", false);


        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
