package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.managers.AudioManager;

import java.util.List;

public class LeaveCommand implements Command {
    private final String HELP = "Usage: !ping";
    public static GuildMusicManager musicManager;

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
                /*
        This works. Leave it.
        Don't. Touch. Any. Part. Of. This. Ever. Again.
         */
        Guild guild = event.getGuild();
        long guildId = Long.parseLong(guild.getId());
        String[] command = event.getMessage().getContentDisplay().split(" ", 2);
        if(CommandPermission.isAdmin(event) || CommandPermission.isOwner(event)){
            musicManager.player.stopTrack();
            guild.getAudioManager().setSendingHandler(null);
            guild.getAudioManager().closeAudioConnection();
        } else {
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + ": Error" +
                    " - Permission denied - Please make sure you are the role [Bot Commander]!").queue();
        }
    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        help_bulder.setTitle("Leave channel",null);
        help_bulder.setDescription("Forces the music bot to stop playing and to leave the voice channel");
        help_bulder.addField("Syntax", "!leave \n!leave --help",true);
        help_bulder.addField("", "{Causes the bot to leave the current channel}\n{Requests help for the bot}",true);
        help_bulder.addField("Permissions","Bot Commander+", false);

        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }

}
