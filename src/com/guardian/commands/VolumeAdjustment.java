package com.guardian.commands;

import com.guardian.Command;
import com.guardian.commands.verification.CommandPermission;
import com.guardian.util.Config;
import com.guardian.util.SchedulerService;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.sql.*;

public class VolumeAdjustment implements Command {

    private Connection connect = null;
    private Statement statement = null;

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) { return true; }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        if (CommandPermission.hasPermission("Bot Commander", event) | CommandPermission.isOwner(event)) {
            if (args.length == 1) {
                int volume = Integer.valueOf(args[0]);
                if (volume <= 100 | volume > 0) {
                    try {
                        connect = DriverManager.getConnection("jdbc:mysql://" + Config.sqlHostname + ":3306/" + Config.sqlUser + "?autoReconnect=true&useSSL=false&serverTimezone=UTC"
                                , Config.sqlUser, Config.sqlPassword);
                        statement = connect.createStatement();

                        PreparedStatement ps = connect.prepareStatement(
                                "UPDATE `guilds` SET bot_volume = ? WHERE gid = " + event.getGuild().getId());
                        ps.setInt(1, volume);
                        ps.executeUpdate();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Updated bot volume to " + volume).queue();
                } else {
                    event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Please enter a value between 0 and 100").queue();
                }
            } else {
                event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Failed to adjust the volume - Syntax Error").queue();
            }
        } else

        {
            event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Failed to get permission").queue();
        }

    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {

    }
}
