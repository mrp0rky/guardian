package com.guardian.commands;

import com.guardian.Command;
import net.dean.jraw.RedditClient;
import net.dean.jraw.http.NetworkException;
import net.dean.jraw.http.UserAgent;
import net.dean.jraw.http.oauth.Credentials;
import net.dean.jraw.http.oauth.OAuthData;
import net.dean.jraw.http.oauth.OAuthException;
import net.dean.jraw.models.Submission;
import net.dean.jraw.paginators.SubredditPaginator;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.awt.*;

public class SubredditImage implements Command {
    private final String HELP = "Usage: !reddit [subreddit]";
    private String[] message;
    RedditClient client;

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        message = event.getMessage().getContentDisplay().split("\\s+");
        for (int i = 0; i < 1; i++) {
            try {
                if (message[i] == "!reddit") {
                    return;
                } else {
                    UserAgent agent = UserAgent.of("desktop", "com.guardian.commands", "v0.01", "Guardian_bot");
                    Credentials credentials = Credentials.script("Guardian_bot", "2vdeN6kFq9j8", "ozpxmQhFlNnadw", "5QE_MY9QsO0r2LilBG8Qv9Q0CAE");
                    client = new RedditClient(agent);
                    try {
                        OAuthData authData = client.getOAuthHelper().easyAuth(credentials);
                        client.authenticate(authData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getRandomSubreddit(event);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Error! You must specify a subreddit to gather data from!").queue();
            }
        }
    }

    public void getRandomSubreddit(MessageReceivedEvent event) {
        SubredditPaginator sp = new SubredditPaginator(client);
        try {
            Submission sub = client.getRandomSubmission(message[1]);
            if (sub.isSelfPost() == false && sub.getUpvoteRatio() > 0.75 && sub.getScore() > 5) {
            /*EmbedBuilder status_builder = new EmbedBuilder();
            status_builder.setColor(event.getMember().getColor());
            status_builder.setTitle("Random post from /r/" + message[1] + "!", null);
            status_builder.setFooter("This is a random post from: " + sub.getPermalink(), null);
            MessageEmbed embed = status_builder.build();
            MessageBuilder mbuilder = new MessageBuilder();
            mbuilder.setEmbed(embed);
            status_builder.addField("[Title] " + sub.getTitle().toString(),"[Upvotes] " + sub.getScore() + " At: " + (sub.getUpvoteRatio() * 100) + "% upvote percentage" , false);
            status_builder.setThumbnail(sub.getThumbnail());
            status_builder.addField("", sub.getUrl().toString(), true);
            status_builder.setImage(sub.getUrl());

            Message message = mbuilder.build();
            event.getChannel().sendMessage(message).queue();*/
                event.getChannel().sendMessage(sub.getUrl()).queue();
            } else {
                getRandomSubreddit(event);
            }
        } catch (NetworkException e) {
            if(e.getResponse().getStatusCode() == 403){
                event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Cannot request subreddit! It is a quarantined subreddit").queue();
            }
        }
    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        return;
    }
}
