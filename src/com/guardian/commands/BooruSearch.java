package com.guardian.commands;

import com.guardian.Command;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.mrporky.booru.core.*;

import java.time.Instant;

public class BooruSearch implements Command {
    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        StringBuilder searchQuery = new StringBuilder();
        for(String arguments : args){
            searchQuery.append(arguments);
        }

        Booru booru = new BooruBuilder(BooruType.GELBOORU).buildClient();
        BooruResponse response = booru.search(searchQuery.toString());
        if(response.success()){
            EmbedBuilder status_builder = new EmbedBuilder();
            status_builder.setColor(event.getMember().getColor());

            status_builder.setTimestamp(Instant.now());
            status_builder.setThumbnail(response.getHighestScore().getURL().toString());
            status_builder.setTitle("Searching Gelbooru for " + searchQuery.toString(), response.getUrl());
            MessageEmbed embed = status_builder.build();
            MessageBuilder mbuilder = new MessageBuilder();
            mbuilder.setEmbed(embed);

            StringBuilder links = new StringBuilder();
            for(BooruPost p : response.topPosts(10)){
                links.append(p.getURL().toString());
                links.append("\n");
            }


            status_builder.addField("Name:", links.toString(), true);
            Message message = mbuilder.build();
            System.out.println("test");
            event.getChannel().sendMessage(message).queue();
        }

    }

    @Override
    public void help(MessageReceivedEvent event) {

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {

    }
}
