package com.guardian;

import com.guardian.commands.*;
import com.guardian.util.BotLoader;
import com.guardian.util.SchedulerService;
import com.guardian.util.SetBotGame;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static String key;   // Key refers to the API secret key
    public static JDA jda;
    static final CommandParser parser = new CommandParser();

    public static HashMap<String, Command> commands = new HashMap<>();  // Hash-map containing all of the possible commands and their corresponding interface

    public Main(){
        BotLoader loader = new BotLoader();
        loader.load();

        // Begin to load in JDA and connect to the API
        try{
            jda = new JDABuilder(AccountType.BOT).setToken(key).buildBlocking();
            jda.addEventListener(new BotListener());
            jda.setAutoReconnect(true);
            new Thread(SchedulerService::new).start();  // Begin the Scheduler thread running the Service class instance
        }catch(Exception e){
            e.printStackTrace();
        }
        // General Commands
        commands.put("ping", new Ping());
        commands.put("status", new Status());
        commands.put("reddit", new SubredditImage());

        commands.put("osu", new OsuAPIRequest());
        commands.put("booru", new BooruSearch());
        commands.put("description", new DescriptionUpdater());
        commands.put("profile", new ProfileInfo());
        commands.put("search" , new SearchSite());
        commands.put("quote" , new AddQuote());

        // Server management commands
        commands.put("setjoin", new JoinChannelUpdate());
        commands.put("addadminrole", new AddAdminRole());

        // Music Bot Commands
        commands.put("play", new PlaySong());
        commands.put("skip", new PlaySong());
        commands.put("queue", new QueueInfo());
        commands.put("volume", new VolumeAdjustment());

        // Moderation commands
        commands.put("ban", new BanUser());
        commands.put("kick", new KickUser());
        commands.put("slowmode", new Slowmode());
        commands.put("prune", new PruneUser());

        //Start the Set-up
        new Thread(() -> {
            new SetBotGame("res/games.txt").updateGame();
        }).start();
    }
    public static void handleCommand(CommandParser.CommandContainer cmd){
        if(commands.containsKey(cmd.invoke)){
            boolean safe = commands.get(cmd.invoke).called(cmd.args, cmd.event);
            if(new ArrayList(Arrays.asList(cmd.args)).contains("--help")){
                commands.get(cmd.invoke).help(cmd.event);
            }else if(safe && !new ArrayList(Arrays.asList(cmd.args)).contains("--help")){
                commands.get(cmd.invoke).action(cmd.args, cmd.event);
                commands.get(cmd.invoke).executed(true, cmd.event);
            } else{
                commands.get(cmd.invoke).executed(safe, cmd.event);
            }
        }
    }
}
