package com.guardian.util;

import com.google.gson.JsonObject;
import net.dv8tion.jda.core.entities.Message;
import org.apache.logging.log4j.core.util.KeyValuePair;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class ImageLayoutGenerator {

    private String imageName;
    private Message message;
    private BufferedImage image;
    private Graphics g;
    private Color containerColor = new Color(0,0,0,127);
    private Color textColor = new Color(245,245,245);
    private Color backgroundColor;
    private File background;
    private LinkedList<KeyValuePair> items = new LinkedList<KeyValuePair>();

    int width = 640, height;

    public ImageLayoutGenerator(){
        height = 20;
    }

    public ImageLayoutGenerator(Color backgroundColor){
        height = 20;
        this.backgroundColor = backgroundColor;
    }

    public ImageLayoutGenerator(String filePath, Color backgroundColor){
        height = 20;
        background = new File(filePath);
        this.backgroundColor = backgroundColor;
    }

    public void addHeader(String headerText){
        items.add(new KeyValuePair(Containers.Header.toString(),headerText));
        height += 45;
    }

    public void addContent(String contentText){
        items.add(new KeyValuePair(Containers.Content.toString(),contentText));
        height += 35;
    }

    public BufferedImage generateImage(){
        BufferedImage retImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        g = retImage.getGraphics();

        if(this.backgroundColor != null){
            g.setColor(backgroundColor);
            g.fillRect(0,0,640,height);
        }
        try {
            for(int i = 0; i < height & this.background != null; i+=360) {
                g.drawImage(ImageIO.read(background), 0, i, 640, 360, null);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int count = 0;
        for(KeyValuePair kvp : items){
            if(kvp.getKey().equals(Containers.Header.toString())){
                g.setColor(containerColor);
                g.fillRect(10,count + 15, width - 20, 35);
                g.setColor(textColor);
                g.setFont(g.getFont().deriveFont(30f));
                g.drawString(kvp.getValue(), 20,   count + 45);
                count+=45;
            }

            if(kvp.getKey().equals(Containers.Content.toString())){
                g.setColor(containerColor);
                g.fillRect(10,count + 10, width - 20, 25);
                g.setColor(textColor);
                g.setFont(g.getFont().deriveFont(20f));
                g.drawString(kvp.getValue(), 20,   count + 30);
                count+=35;
            }

        }

        return retImage;
    }
    private enum Containers{
        Header, Content;
    }

}
