package com.guardian.util;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class RestReturn {
    public RestReturn() {

    }

    public JsonObject getRest(String url) {
        try {
            URL url_working = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) url_working.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }
            output = sb.toString();
            JsonParser parser = new JsonParser();
            JsonObject obj = parser.parse(output).getAsJsonArray().get(0).getAsJsonObject();
            connection.disconnect();
            return obj;

        } catch (MalformedURLException e) {
            System.err.println("Error with REST API! Malformed URL detected!");
        } catch (IOException e) {
            System.err.println("Failed to open connection with specific URL! Check if the URL is valid or if the site is down!");
        }
        return null;
    }

    private static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    /**
     * Converts a JSONArray to a list of type object
     * @param array
     * @return arrayFromJSON
     * @throws JSONException
     */

    private static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(Object obj : array) {
            // Create a temp object to hold the JSON generic object
            Object value = obj;
            // Checks if the value is that of a JSON object or array
            if(value instanceof JSONArray) {
                value = toList((JSONArray) obj);
            } else if(value instanceof JSONObject) {
                value = toMap((JSONObject) obj);
            }
            // Adds the value to the list
            list.add(value);
        }
        return list;
    }

    public String getRestYoutube(String url) {
        try {
            URL url_working = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) url_working.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String output;
            while ((output = br.readLine()) != null) {
                if (output.contains("videoId")) {
                    return output;
                }
            }

            connection.disconnect();
        } catch (MalformedURLException e) {
            System.err.println("Error with REST API! Malformed URL detected!");
        } catch (IOException e) {
            System.err.println("Failed to open connection with specific URL! Check if the URL is valid or if the site is down!");
        }
        return "NULL";
    }
}
