package com.guardian.util;

import java.sql.*;

public class BotSQLQuery {
    private Connection connect = null;
    private Statement statement = null;

    public String retriveSQL(String selection, String table, String where, String data){
        String retrive = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true"
                            , Config.sqlUser, Config.sqlPassword);
            statement = connect.createStatement();
            String queryCheck = "SELECT "+ selection +" from " + table + " WHERE " + where + "=(?)";
            PreparedStatement ps = connect.prepareStatement(queryCheck);
            ps.setString(1, "%" + data + "%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                retrive = rs.getString(where);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            retrive = "The SQL query failed! Error " + e.getErrorCode();
            e.printStackTrace();
        }
        return retrive;
    }

    public void setSQLEntry(String table, String column, String newValue, String limitColumn, String limitValue){
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true"
                            , Config.sqlUser, Config.sqlPassword);
            statement = connect.createStatement();
            String queryCheck = "UPDATE "+ table + " SET " + column + "=(?) WHERE " + limitColumn + "=(?)";
            PreparedStatement ps = connect.prepareStatement(queryCheck);
            ps.setString(1, "%" + newValue + "%");
            ps.setString(2, "%" + limitValue + "%");
            ResultSet rs = ps.executeQuery();
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
