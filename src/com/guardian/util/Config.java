package com.guardian.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Config {
    private final static Logger LOGGER = Logger.getLogger(Config.class.getName());
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    public static boolean DISABLESQL = true;
    private String filename;
    public static String sqlHostname, sqlPassword , sqlUser, sqlDatabaseName, sshUser, sshServer, sshPassword= "";
    /*
    This file is not for configuring the settings for the bot - This is to read the config.txt file in the base directory
     DO NOT EDIT THIS - It may destroy the links to the databases.
     */

    public Config(String filename){
        this.filename = filename;
    }

    public void load(){
        Scanner s = null;
        try {
            s = new Scanner(new File(filename));
            ArrayList<String> list = new ArrayList<String>();
            for(int i = 0; s.hasNext(); i++){
                list.add(s.next());
            }

            if(list.contains("SQL-DBNAME")){
                int temp_index = list.indexOf("SQL-DBNAME") + 1;
                sqlDatabaseName = list.get(temp_index);
            }
            if(list.contains("SQL-HOSTNAME")){
                int temp_index = list.indexOf("SQL-HOSTNAME") + 1;
                sqlHostname = list.get(temp_index);
            }

            if(list.contains("SQL-PASSWORD")){
                int temp_index = list.indexOf("SQL-PASSWORD") + 1;
                sqlPassword = list.get(temp_index);
            }

            if(list.contains("SQL-USER")){
                int temp_index = list.indexOf("SQL-USER") + 1;
                sqlUser = list.get(temp_index);
            }

            if(list.contains("SSH-USER")){
                int temp_index = list.indexOf("SSH-USER") + 1;
                sshUser = list.get(temp_index);
            }

            if(list.contains("SSH-PASS")){
                int temp_index = list.indexOf("SSH-PASS") + 1;
                sshPassword = list.get(temp_index);
            }

            if(list.contains("SSH-SERVER")){
                int temp_index = list.indexOf("SSH-SERVER") + 1;
                sshServer = list.get(temp_index);
            }
            s.close();
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.WARNING, "Config file not found! Any settings will not be applied!");
        }
        LOGGER.log(Level.INFO, "Successfully loading the configuration settings");
    }
}
