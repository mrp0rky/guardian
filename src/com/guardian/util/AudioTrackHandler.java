package com.guardian.util;

import com.guardian.commands.GuildMusicManager;
import com.guardian.commands.PlaySong;
import net.dv8tion.jda.core.entities.Guild;

public class AudioTrackHandler {
    Guild guild;
    public AudioTrackHandler(Guild guild){
        this.guild = guild;
    }

    public synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = PlaySong.musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(PlaySong.playerManager);
            PlaySong.musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }
}
