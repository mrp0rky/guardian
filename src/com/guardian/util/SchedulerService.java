package com.guardian.util;


import com.guardian.Main;
import com.guardian.commands.GuildMusicManager;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.VoiceChannel;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SchedulerService {

    private Connection connect = null;
    private static final ScheduledExecutorService exec = Executors.newScheduledThreadPool(2);
    private final static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(Config.class.getName());

    public SchedulerService() {
        /*
        Begins the initiation of the scheduler services - This scope defines the different things Guardian will periodically poke.
         */
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // Guild updater -> On 12 hour wait as it is quite intensive past around 50 guilds and requires extensive API pulls -> Let's not annoy Discord with too many requests
                manageGuilds();
            }
        }, 0, 12, TimeUnit.HOURS); // Defines the time period that the bot should wait before it runs the Scheduler.

        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // Changes the display "Playing X" that appears on the bot in the right hand sidebar
                changeGame();
            }
        }, 0, 10, TimeUnit.MINUTES); // Defines the time period that the bot should wait before it runs the Scheduler.

        /*exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                exitVoiceChannel();
            }
        }, 0, 5, TimeUnit.SECONDS);*/
    }

    private void exitVoiceChannel(){
        JDA jda = Main.jda;
        for(VoiceChannel channel : jda.getVoiceChannels()){
            for(Member member :channel.getMembers()){
                if(member.getUser().getId().equals(jda.getSelfUser().getId())){
                    AudioTrackHandler handler = new AudioTrackHandler(channel.getGuild());
                    final GuildMusicManager musicManager = handler.getGuildAudioPlayer(channel.getGuild());
                    LinkedList<AudioTrack> queue = musicManager.scheduler.getQueue();
                    if(queue.isEmpty() && musicManager.player.getPlayingTrack() == null){
                        channel.getGuild().getAudioManager().closeAudioConnection();
                    }
                }
            }

        }
    }

    private void changeGame() {
        SetBotGame botGame = new SetBotGame("res/games.txt");botGame.updateGame();
    }

    public static void alertError(String service, int errorlev) {

        if (errorlev == 0) {
            System.err.println("Error starting " + service + "! Critical error!");
        } else if (errorlev == 1) {
            System.err.println("Error starting " + service + "! Alert error!");
        } else if (errorlev == 2) {
            System.err.println("Error starting " + service + "! Info error!");
        }
    }

    public void manageGuilds() {
        try {
            List guilds = Main.jda.getGuilds();
            for (int i = 0; i < guilds.size(); i++) {
                String guild_string = guilds.get(i).toString();
                String guild_id = guild_string.substring(guild_string.indexOf("(") + 1, guild_string.indexOf(")"));
                String owner_id = Main.jda.getGuildById(String.valueOf(guild_id)).getOwner().getUser().getId();
                int usercount = Main.jda.getGuildById(String.valueOf(guild_id)).getMembers().size();
                String guild_name = guild_string.substring(guild_string.indexOf(":") + 1, guild_string.indexOf("("));

                /* Get the text channels of the server */
                // Get the list of guilds from JDA
                List channelList_list = Main.jda.getGuildById(guild_id).getTextChannels();
                // Convert this into a string -> TODO convert to stringbuilder
                String channelList = channelList_list.toString();
                //Remove the excess data to just get the guild names -> Discord only allows basic alphanumerical values and '-' sign -> No need to worry about users input effect
                Pattern p = Pattern.compile(Pattern.quote(":") + "(.*?)" + Pattern.quote("("));
                Matcher m = p.matcher(channelList);

                List channelList_precompile = new LinkedList();
                int count = 0;
                // While loop to add the values from the matcher to a new LinkedList from a string can be built easier
                while (m.find()) {
                    channelList_precompile.add(count, m.group(1));
                    count++;
                }
                // Build the string -> for loop does up to the second to last value, and the last is added the line after so it formats correctly -> It's a hack.
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < channelList_precompile.size() - 1; j++) {
                    sb.append(channelList_precompile.get(j)).append(",");
                }
                sb.append(channelList_precompile.get(channelList_precompile.size() - 1));
                channelList = sb.toString();

                    /*  Get the data of the roles that the guild has active */
                List roles_list = Main.jda.getGuildById(guild_id).getRoles();

                String roles = roles_list.toString().replace("[", "").replace("]", "").replaceAll(", ", "\n");

                Pattern role_p = Pattern.compile(Pattern.quote(":") + "(.*?)" + Pattern.quote("("));
                Matcher role_m = role_p.matcher(roles);

                List roles_comp = new LinkedList();
                int role_c = 0;
                // While loop to add the values from the matcher to a new LinkedList from a string can be built easier

                while (role_m.find()) {
                    roles_comp.add(role_c, role_m.group(1));
                    role_c++;

                }

                StringBuilder role_sb = new StringBuilder();
                for (int x = 0; x < roles_comp.size() - 1; x++) {
                    role_sb.append(roles_comp.get(x)).append(",");
                }
                role_sb.append(roles_comp.get(roles_comp.size() - 1));
                roles = role_sb.toString();
                // Update this in the database

                try {
                    connect = DriverManager
                            .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=false&serverTimezone=UTC"
                                    , Config.sqlUser, Config.sqlPassword);
                    // Checks if the entry already exists to avoid duplicate entries into the DB
                    Statement stmt = connect.createStatement();
                    ResultSet result = stmt.executeQuery("SELECT ownerid FROM `guilds` WHERE gid = " + guild_id);
                    if (!result.next()) {
                        // Data entry does not exist -> Creating new entry:
                        PreparedStatement ps = connect.prepareStatement(
                                "INSERT INTO `guilds` SET gid = ?, ownerid =?, usercount = ?, guild_name = ?, text_channels = ?, user_roles = ?");
                        ps.setString(1, guild_id);
                        ps.setString(2, owner_id);
                        ps.setInt(3, usercount);
                        ps.setString(4, guild_name);
                        ps.setString(5, channelList);
                        ps.setString(6, roles);
                        ps.executeUpdate();
                        connect.close();
                    } else {
                        // Data entry exists -> Updating the entry
                        PreparedStatement ps = connect.prepareStatement(
                                "UPDATE `guilds` SET gid = ?, ownerid =?, usercount = ?, guild_name = ?, text_channels = ?, user_roles = ?" + " WHERE gid=" + guild_id);
                        ps.setString(1, guild_id);
                        ps.setString(2, owner_id);
                        ps.setInt(3, usercount);
                        ps.setString(4, guild_name);
                        ps.setString(5, channelList);
                        ps.setString(6, roles);
                        ps.executeUpdate();
                        connect.close();
                    }
                } catch (SQLException e) {
                    System.err.println("Failed to connect to SQL server or malformed query! Scheduler has exited!");
                    e.printStackTrace();
                    Thread.currentThread().stop();
                }
            }
            System.out.println("Updated Guilds");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
