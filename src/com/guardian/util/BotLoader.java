package com.guardian.util;

import com.guardian.Main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class BotLoader {
    private Logger LOGGER;
    public BotLoader(){
        LOGGER = Logger.getLogger(Config.class.getName());
    }

    public void load(){
        // Load the bot private key from the file
        LOGGER.log(Level.INFO, "Loading the bot private key from file.");
        File key_file = new File("key.txt");
        FileReader reader = null;
        try {
            reader = new FileReader(key_file);
            char[] chars = new char[(int) key_file.length()];
            reader.read(chars);
            Main.key = new String(chars);
            reader.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed to load the key from the file! Aborting!");
            System.exit(0);
        } finally {
            if(reader !=null){
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Failed to load the key from the file! Aborting!");
                    System.exit(0);
                }
            }
        }
        // Begin to load configuration for the bot
        LOGGER.log(Level.INFO, "Beginning to load the configuration files");
        Config config = new Config("config.txt");
        config.load();
        LOGGER.log(Level.INFO, "Finished loading config!");
    }
}
