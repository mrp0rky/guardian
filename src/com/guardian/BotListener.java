package com.guardian;

import com.guardian.commands.QuoteManager;
import com.guardian.commands.Slowmode;
import com.guardian.util.Config;
import com.guardian.util.SchedulerService;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.requests.RequestFuture;
import net.dv8tion.jda.core.requests.RestAction;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class BotListener extends ListenerAdapter {
    private static final Logger logger = Logger.getAnonymousLogger();
    private boolean slowmode_bol = false;

    // Variables for adding XP to user
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private LinkedList<String> guildid;

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        new JoinEvent().onJoin(event);
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        slowmode_bol = false;   // Resets slowmode so that the message is always sent if it should be

        if (!event.getAuthor().isBot()) {
            Message message = event.getMessage();
            String botname = "@" + event.getJDA().getSelfUser().getName();
            char msg = 0;

            // Adds XP for user interaction
            addXP(event);

            // Slowmode checking
            if (Slowmode.slowmode_channels.contains(event.getTextChannel().getId())) {
                slowMode(event);
            }

            if (!slowmode_bol) {                  // If slowmode is on, do not send the message to the other handlers -> This is to stop bot spam
                try {
                    msg = message.getContentDisplay().charAt(0);
                } catch (StringIndexOutOfBoundsException ignored) {}

                if (event.isFromType(ChannelType.PRIVATE)) {
                    System.out.printf("[PM] %s: %s\n", event.getAuthor().getName(),
                            event.getMessage().getContentDisplay());
                } else if (msg == '!') {
                    Main.handleCommand(Main.parser.parse(event.getMessage().getContentDisplay().toLowerCase(), event));
                }
            }
        }

        if(event.getMessage().getType().equals(MessageType.CHANNEL_PINNED_ADD)){
            RestAction<List<Message>> getPinned = event.getTextChannel().getPinnedMessages();
            RequestFuture<List<Message>> getPinnedFuture = getPinned.submit();
            try {
                List<Message> pinnedMessages = getPinnedFuture.get();
                QuoteManager manager = new QuoteManager();
                manager.addQuote(pinnedMessages.get(0));

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public void slowMode(MessageReceivedEvent event) {
        long message_time = event.getMessage().getCreationTime().toEpochSecond();
        TextChannel channel = event.getTextChannel();
        channel.getHistory().retrievePast(100).queue((List<Message> messages) -> {
            for (int i = 1; i < messages.size(); i++) {
                if (messages.get(i).getAuthor().equals(event.getAuthor())) {
                    long search_message_time = messages.get(i).getCreationTime().toEpochSecond();
                    int length = Slowmode.slowmode_length.get(Slowmode.slowmode_channels.indexOf(event.getTextChannel().getId()));
                    if ((message_time - search_message_time) < length) {
                        slowmode_bol = true;
                        if(!event.getMessage().getContentDisplay().contains("!slowmode")) {
                            event.getMessage().delete().queue();
                        }
                    }
                }
            }
        });
    }

    public void addXP(MessageReceivedEvent event) {
        String profile_pic = "https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png";
        // Retrieving profile picture URL
        if (event.getAuthor().getAvatarUrl() != null) {
            profile_pic = event.getAuthor().getAvatarUrl().toString();
        }
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connect = DriverManager.getConnection("jdbc:mysql://" + Config.sqlHostname +":3306/"+Config.sqlUser+"?autoReconnect=true&useSSL=false&serverTimezone=UTC"
                            , Config.sqlUser, Config.sqlPassword);
            statement = connect.createStatement();
            //statement.executeUpdate("INSERT INTO `users` (`id`, `username`, `description`, `xp`) VALUES ("+event.getAuthor().getId()+",'" +event.getAuthor().getName().toString()+"', 'Test', '0')");

            // Checks if there is already an entry for that user
            String queryCheck = "SELECT * from users WHERE id = " + Long.parseLong(event.getAuthor().getId());
            Statement qc = connect.createStatement();
            ResultSet qc_rs = statement.executeQuery(queryCheck);
            if (qc_rs.absolute(1)) {
                //statement.execute("UPDATE users SET xp=xp+1 WHERE id =" + Long.parseLong(event.getAuthor().getId()));
                ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE id = " + Long.parseLong(event.getAuthor().getId()));
                String desc = null;
                int xp = 0;
                while (rs.next()) {
                    desc = rs.getString("description");
                    xp = rs.getInt("xp");
                }
                PreparedStatement ps = connect.prepareStatement(
                        "UPDATE `users` SET username= ?, xp=?, profilepic = ?" + " WHERE id=" + event.getAuthor().getId());
                ps.setString(1, event.getAuthor().getName());
                ps.setInt(2, xp + 1);
                ps.setString(3, profile_pic);
                ps.executeUpdate();
                connect.close();
                return;

            } else {
                PreparedStatement ps = connect.prepareStatement(
                        "INSERT INTO users (id, username, description, xp, profilepic)" + "VALUES (?, ?, ? ,?,?)");
                ps.setLong(1, Long.parseLong(event.getAuthor().getId()));
                ps.setString(2, event.getAuthor().getName());
                ps.setString(3, "Default Description");
                ps.setInt(4, 0);
                ps.setString(5, profile_pic);
                ps.executeUpdate();
            }


        } catch (SQLException e) {
            SchedulerService.alertError("User update" , 0);
        }
    }

    @Override
    public void onReady(ReadyEvent event) {
        logger.info("Logged in as: " + event.getJDA().getSelfUser().getName());
    }
}
