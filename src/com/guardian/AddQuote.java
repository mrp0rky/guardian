package com.guardian;

import com.guardian.commands.QuoteManager;
import com.guardian.util.Config;
import com.guardian.util.SQLQuery;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class AddQuote implements Command {
    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return true;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        if(args.length >= 2){
            String nameAdded = args[0].toLowerCase();
            String nameOfMention = ("@" + event.getMessage().getMentionedUsers().get(0).getName()).toLowerCase();

            if(event.getMessage().getMentionedUsers().size() == 1 && nameAdded.equals(nameOfMention)) {
                QuoteManager manager = new QuoteManager();
                String quote = event.getMessage().getContentRaw().replace("!quote <@" + event.getMessage()
                        .getMentionedUsers().get(0).getId() + "> ", "");
                manager.addQuote(event.getMessage().getMentionedUsers().get(0), quote, event.getGuild(), event.getMessage());
                event.getTextChannel().sendMessage(event.getAuthor().getAsMention() + " - Added quote: \"" + quote + "\"!").queue();
            }else if(args[0].equalsIgnoreCase("from") && event.getMessage().getMentionedUsers().size() == 1 && args.length <= 3){
                SQLQuery query = new SQLQuery(Config.sqlHostname, Config.sqlDatabaseName, Config.sqlUser, Config.sqlPassword, 3306);
                String[] values = {event.getMessage().getMentionedUsers().get(0).getId()};
                ResultSet rs = query.query("SELECT * from quotes where uid = ?", values);
                LinkedList<String> messages = new LinkedList<>();

                try {
                    while (rs.next()) {
                        messages.add(rs.getString("message_content"));
                    }
                }catch (SQLException e){

                }

                System.out.println(event.getMessage().getContentRaw().contains("--random"));
                if(event.getMessage().getContentRaw().contains("--random")){
                    System.out.println("test");
                    event.getTextChannel().sendMessage(event.getMessage().getMentionedUsers().get(0).getAsMention() + ": " + messages.get((int) (Math.random() * messages.size()))).queue();
                }else{
                    event.getTextChannel().sendMessage(event.getMessage().getMentionedUsers().get(0).getAsMention() + ": " + messages.get(0)).queue();
                }
            }
        }else{
            event.getTextChannel().sendMessage("Please format the message in the correct way! !quote [user-ping] message").queue();
        }
    }

    @Override
    public void help(MessageReceivedEvent event) {
        EmbedBuilder help_bulder = new EmbedBuilder();
        help_bulder.setColor(event.getMember().getColor());
        help_bulder.setTitle("Quote storing", null);
        help_bulder.setDescription("Can store and respond with either the last user's quotes or a random one\nQuotes can also be added by pinning a message!");
        help_bulder.addField("Syntax", "!quote [user tag] [quote] \n!quote from [user tag] \n !quote from [user tag] --random", true)
        .addField("", "{Adds a new quote}\n{Gets the last quote that the bot has stored}\n {Gets a random quote from the bot}", true)
        .addField("Permissions", "Default", false);

        // Build the Message
        MessageEmbed embed = help_bulder.build();
        MessageBuilder mbuilder = new MessageBuilder();
        mbuilder.setEmbed(embed);
        Message message_compiled = mbuilder.build();

        // Output the message
        event.getChannel().sendMessage(message_compiled).queue();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {

    }
}
