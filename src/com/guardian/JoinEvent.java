package com.guardian;

import com.guardian.util.BotSQLQuery;
import com.guardian.util.Config;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.exceptions.PermissionException;
import net.dv8tion.jda.core.managers.GuildController;
import net.dv8tion.jda.core.managers.GuildManager;

import java.sql.*;
import java.util.List;

public class JoinEvent extends BotListener{
    private Connection connect = null;
    private Statement statement = null;
    String welcome_channel;
    String assign_role;
    public void onJoin(GuildMemberJoinEvent event){
        if(!Config.DISABLESQL) {
            BotSQLQuery sqlQuery = new BotSQLQuery();
            welcome_channel = sqlQuery.retriveSQL("welcome_channel", "guilds", "gid", event.getGuild().getId());
            assign_role = sqlQuery.retriveSQL("join_role", "guilds", "gid", event.getGuild().getId());
        }
        /*try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://"+Config.sqlHostname+":3306/"+Config.sqlDatabaseName+"?autoReconnect=true&useSSL=true"
                            , Config.sqlUser, Config.sqlPassword);
            statement = connect.createStatement();

            ResultSet rs = statement.executeQuery("SELECT welcome_channel, join_role FROM guilds WHERE gid = " + event.getGuild().getId());

            while (rs.next()) {
                welcome_channel = rs.getString("welcome_channel");
                assign_role = rs.getString("join_role");
            }
            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
        if(welcome_channel != null) {
            TextChannel channel = event.getGuild().getTextChannelsByName(welcome_channel, true).get(0);
            channel.sendMessage("Welcome to " + event.getGuild().getName().toString() + ", " + event.getMember().getUser().getAsMention() + "!").queue();
        }

        if(assign_role != null && assign_role != "@everyone"){
            GuildController guildController = event.getGuild().getController();
            List<Role> role = event.getGuild().getRolesByName(assign_role, true);
            try {
                guildController.addRolesToMember(event.getMember(), role.get(0)).queue();
            }catch (PermissionException exception){
                event.getGuild().getTextChannelsByName("mod-log", false).get(0).sendMessage(event.getGuild().getOwner().getAsMention() + " - Attempted to add " + event.getMember().getAsMention() +
                        " to role: " + assign_role + " - Can't modify a role with higher or equal highest role than the bot!").queue();
            }
        }

        event.getMember().getUser().openPrivateChannel().queue(PrivateChannel -> PrivateChannel.sendMessage("Hello " + event.getMember().getEffectiveName() +"! Welcome to " + event.getGuild().getName()
                + "! This Discord uses Guardian as a management bot and a general purpose bot. Due to this, some end user data has to be stored. As such, by posting on this guild you agree to" +
                " allow for Guardian to store user data such as usernames, and profile pictures. Privacy is a big concern, and as such, your data will never be shared with" +
                " third parties. For a more detailed rundown of the privacy policy, please visit the [website] or type !privacy").queue());
    }
}
