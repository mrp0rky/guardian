package com.guardian;
/*
This is the main initialisation class -> Apart from being the Init, this does nothing but start the main thread.
 */
public class InitMain{
    public static void main(String[]args){
        new Thread(Main::new).start();
    }
}
